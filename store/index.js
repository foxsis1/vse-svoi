import axios from 'axios'

export const state = () => ({
  products: [],
  tours: [],
  variations: [],
  errors: null


})
export const mutations = {
  SET_PRODUCT_TO_STATE: (state, products) => {
    state.products = products
  },

  //TOURS
  SET_TOURS_TO_STATE: (state, tours) => {
    state.tours = tours
  },

  //VARIATIONS
  SET_VARIATIONS_TO_STATE: (state, variations) => {
    state.variations = variations
  },

  
}

export const actions = {
  
  GET_PRODUCT_FROM_API ({ commit }) {
    return axios('https://sharikoff-stav.store/wp-json/wc/v3/products', {
      auth: {
        username: 'ck_a1e76b0c4740131ec2a95e3710e1d143bc39758d',
        password: 'cs_37bfd52d190b6389298d594b76c101c48a4f252e'
      },
      params: {
        per_page: 4,
        category: 322
      }
    })
      .then(products => {
        commit('SET_PRODUCT_TO_STATE', products)
        return products
      })
      .catch(e => {
        this.errors.push(e)
        return errors
      })
  },

  //VARIATIONS
  GET_VARIATIONS_FROM_API ({ commit }) {
    return axios(
      'https://sharikoff-stav.store/wp-json/wc/v3/products/39/variations',
      {
        auth: {
          username: 'ck_a1e76b0c4740131ec2a95e3710e1d143bc39758d',
          password: 'cs_37bfd52d190b6389298d594b76c101c48a4f252e'
        },
        params: {
          per_page: 100
        }
      }
    )
      .then(variations => {
        commit('SET_VARIATIONS_TO_STATE', variations.data)
        return variations
      })
      .catch(e => {
        this.errors.push(e)
        return errors
      })
  },

 
  //TOURS
  GET_TOURS_FROM_API ({ commit }) {
    return axios('https://sharikoff-stav.store/wp-json/wc/v3/products', {
      auth: {
        username: 'ck_a1e76b0c4740131ec2a95e3710e1d143bc39758d',
        password: 'cs_37bfd52d190b6389298d594b76c101c48a4f252e'
      },
      params: {
        category: 227,
      }
    })
      .then(tours => {
        commit('SET_TOURS_TO_STATE', tours.data)
        return tours
      })
      .catch(e => {
        this.errors.push(e)
        return errors
      })
  }
}

export const getters = {
  PRODUCTS (state) {
    return state.products
  },

  //TOURS
  TOURS (state) {
    return state.tours
  },

  //VARIATIONS
  VARIATIONS (state) {
    return state.variations
  },

  //VARIATIONS PLACE
  PLACE (state) {
    return state.place
  },

}
